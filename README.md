# ⇅ Internet Speed Meter
A Simple and Minimal Internet Speed Meter Extension for Gnome Shell.

### Screenshot

![Screen_Two.png](https://s26.postimg.cc/yhgm1dleh/Screen_Two.png)

### Installation

###### Gnome Extensions: [Internet Speed Meter](https://extensions.gnome.org/extension/1461/internet-speed-meter/) 

### Installation from Git Source

##### 1. If previous version was installed, remove them first.

`sudo rm -rf /usr/share/gnome-shell/extensions/Internet-Speed-Meter@TH3L0N3C0D3R`

`rm -rf ~/.local/share/gnome-shell/extensions/Internet-Speed-Meter@TH3L0N3C0D3R`

##### 2. If `Gnome Tweak Tool` is not installed, install it.

Fedora/RHEL/CentOS: `sudo yum install gnome-tweak-tool`

Debian/Ubuntu: `sudo apt install gnome-tweak-tool`

Arch/Manjaro: `sudo pacman -S gnome-tweaks`

##### 3. Install Internet Speed Meter

`git clone https://gitlab.com/TH3L0N3C0D3R/Internet-Speed-Meter.git ~/.local/share/gnome-shell/extensions/Internet-Speed-Meter@TH3L0N3C0D3R`


##### 4. Refresh gnome shell:
Hit `Alt + F2`

Type `r` and hit `Enter`

##### 5. Enable extension in `Gnome Tweak Tool`.

### Compatibility

This extension has been tested with Gnome 3.20+.

### Public License
GPLv3.0
